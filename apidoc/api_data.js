define({ "api": [
  {
    "type": "post",
    "url": "/announcement/add",
    "title": "用户公告新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addannouncement",
    "group": "announcement",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_id",
            "description": "<p>公告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_type",
            "description": "<p>公告类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_content",
            "description": "<p>公告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_name",
            "description": "<p>公告内标题</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/announcement/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/announcement.js",
    "groupTitle": "announcement"
  },
  {
    "type": "post",
    "url": "/announcement/delete",
    "title": "用户公告删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteannouncement",
    "group": "announcement",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/announcement/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/announcement.js",
    "groupTitle": "announcement"
  },
  {
    "type": "post",
    "url": "/announcement/queryList",
    "title": "用户公告查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListannouncement",
    "group": "announcement",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/announcement/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/announcement.js",
    "groupTitle": "announcement"
  },
  {
    "type": "post",
    "url": "/announcement/query",
    "title": "用户公告查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryannouncement",
    "group": "announcement",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_id",
            "description": "<p>公告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_type",
            "description": "<p>公告类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_content",
            "description": "<p>公告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_name",
            "description": "<p>公告内标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"announcement_id\":\"公告ID\"\n        \"announcement_type\":\"公告类型\"\n        \"announcement_content\":\"公告内容\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"announcement_name\":\"公告内标题\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/announcement/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/announcement.js",
    "groupTitle": "announcement"
  },
  {
    "type": "post",
    "url": "/announcement/update",
    "title": "用户公告更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateannouncement",
    "group": "announcement",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_id",
            "description": "<p>公告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_type",
            "description": "<p>公告类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_content",
            "description": "<p>公告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "announcement_name",
            "description": "<p>公告内标题</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/announcement/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/announcement.js",
    "groupTitle": "announcement"
  },
  {
    "type": "post",
    "url": "/conference/add",
    "title": "会议日程报告新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addconference",
    "group": "conference",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "startDate",
            "description": "<p>会议开始日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "endDate",
            "description": "<p>会议结束日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>会议内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "site",
            "description": "<p>会议地点</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告种类ID 大会报告/分会报告/发布报告/作品集</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "is_state",
            "description": "<p>是否开始 1开始 0未开始</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>会议房间号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "us_assembly",
            "description": "<p>关联的大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "plenary_id",
            "description": "<p>大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "videoUrl",
            "description": "<p>直播地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "num",
            "description": "<p>房间最大人数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>房间密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tools",
            "description": "<p>直播工具 --非必填 默认room</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_number",
            "description": "<p>会议编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "articleUrl",
            "description": "<p>文章地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "room_name",
            "description": "<p>房间名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "compere_name",
            "description": "<p>会议主持人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/conference/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/conference.js",
    "groupTitle": "conference"
  },
  {
    "type": "post",
    "url": "/conference/delete",
    "title": "会议日程报告删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteconference",
    "group": "conference",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conference_id",
            "description": "<p>会议ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/conference/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/conference.js",
    "groupTitle": "conference"
  },
  {
    "type": "post",
    "url": "/conference/queryList",
    "title": "会议日程报告查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListconference",
    "group": "conference",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/conference/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/conference.js",
    "groupTitle": "conference"
  },
  {
    "type": "post",
    "url": "/conference/query",
    "title": "会议日程报告查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryconference",
    "group": "conference",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conference_id",
            "description": "<p>会议ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "startDate",
            "description": "<p>会议开始日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "endDate",
            "description": "<p>会议结束日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>会议内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "site",
            "description": "<p>会议地点</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告种类ID 大会报告/分会报告/发布报告/作品集</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "is_state",
            "description": "<p>是否开始 1开始 0未开始</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>会议房间号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "us_assembly",
            "description": "<p>关联的大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "plenary_id",
            "description": "<p>大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "videoUrl",
            "description": "<p>直播地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "num",
            "description": "<p>房间最大人数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>房间密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tools",
            "description": "<p>直播工具 --非必填 默认room</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_number",
            "description": "<p>会议编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "articleUrl",
            "description": "<p>文章地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "room_name",
            "description": "<p>房间名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "compere_name",
            "description": "<p>会议主持人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"conference_id\":\"会议ID\"\n        \"startDate\":\"会议开始日期\"\n        \"endDate\":\"会议结束日期\"\n        \"content\":\"会议内容\"\n        \"site\":\"会议地点\"\n        \"type\":\"报告种类ID 大会报告/分会报告/发布报告/作品集\"\n        \"serial_id\":\"报告ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"is_state\":\"是否开始 1开始 0未开始\"\n        \"in_then_room\":\"会议房间号\"\n        \"us_assembly\":\"关联的大会ID -弃用\"\n        \"plenary_id\":\"大会ID -弃用\"\n        \"videoUrl\":\"直播地址\"\n        \"num\":\"房间最大人数\"\n        \"password\":\"房间密码\"\n        \"tools\":\"直播工具 --非必填 默认room\"\n        \"serial_number\":\"会议编号\"\n        \"articleUrl\":\"文章地址\"\n        \"room_name\":\"房间名称\"\n        \"compere_name\":\"会议主持人\"\n        \"theme_id\":\"主题ID\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/conference/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/conference.js",
    "groupTitle": "conference"
  },
  {
    "type": "post",
    "url": "/conference/update",
    "title": "会议日程报告更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateconference",
    "group": "conference",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "conference_id",
            "description": "<p>会议ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "startDate",
            "description": "<p>会议开始日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "endDate",
            "description": "<p>会议结束日期</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>会议内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "site",
            "description": "<p>会议地点</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告种类ID 大会报告/分会报告/发布报告/作品集</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "is_state",
            "description": "<p>是否开始 1开始 0未开始</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>会议房间号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "us_assembly",
            "description": "<p>关联的大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "plenary_id",
            "description": "<p>大会ID -弃用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "videoUrl",
            "description": "<p>直播地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "num",
            "description": "<p>房间最大人数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>房间密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tools",
            "description": "<p>直播工具 --非必填 默认room</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_number",
            "description": "<p>会议编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "articleUrl",
            "description": "<p>文章地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "room_name",
            "description": "<p>房间名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "compere_name",
            "description": "<p>会议主持人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/conference/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/conference.js",
    "groupTitle": "conference"
  },
  {
    "type": "post",
    "url": "/confernce_type/add",
    "title": "报告分类新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addconfernce_type",
    "group": "confernce_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type_name",
            "description": "<p>报告种类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/confernce_type/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/confernce_type.js",
    "groupTitle": "confernce_type"
  },
  {
    "type": "post",
    "url": "/confernce_type/delete",
    "title": "报告分类删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteconfernce_type",
    "group": "confernce_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "confernce_type_id",
            "description": "<p>报告种类ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/confernce_type/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/confernce_type.js",
    "groupTitle": "confernce_type"
  },
  {
    "type": "post",
    "url": "/confernce_type/queryList",
    "title": "报告分类查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListconfernce_type",
    "group": "confernce_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/confernce_type/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/confernce_type.js",
    "groupTitle": "confernce_type"
  },
  {
    "type": "post",
    "url": "/confernce_type/query",
    "title": "报告分类查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryconfernce_type",
    "group": "confernce_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "confernce_type_id",
            "description": "<p>报告种类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type_name",
            "description": "<p>报告种类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"confernce_type_id\":\"报告种类ID\"\n        \"type_name\":\"报告种类名称\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/confernce_type/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/confernce_type.js",
    "groupTitle": "confernce_type"
  },
  {
    "type": "post",
    "url": "/confernce_type/update",
    "title": "报告分类更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateconfernce_type",
    "group": "confernce_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "confernce_type_id",
            "description": "<p>报告种类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type_name",
            "description": "<p>报告种类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/confernce_type/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/confernce_type.js",
    "groupTitle": "confernce_type"
  },
  {
    "type": "post",
    "url": "/exchange_collect/add",
    "title": "交流圈收藏新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_collect",
    "group": "exchange_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户userId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告类型</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_collect/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_collect.js",
    "groupTitle": "exchange_collect"
  },
  {
    "type": "post",
    "url": "/exchange_collect/delete",
    "title": "交流圈收藏删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_collect",
    "group": "exchange_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_collect_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_collect/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_collect.js",
    "groupTitle": "exchange_collect"
  },
  {
    "type": "post",
    "url": "/exchange_collect/queryList",
    "title": "交流圈收藏查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_collect",
    "group": "exchange_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_collect/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_collect.js",
    "groupTitle": "exchange_collect"
  },
  {
    "type": "post",
    "url": "/exchange_collect/query",
    "title": "交流圈收藏查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_collect",
    "group": "exchange_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_collect_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户userId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_collect_id\":\"\"\n        \"exchange_conter_id\":\"文章ID\"\n        \"userId\":\"用户userId\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"type\":\"报告类型\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_collect/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_collect.js",
    "groupTitle": "exchange_collect"
  },
  {
    "type": "post",
    "url": "/exchange_collect/update",
    "title": "交流圈收藏更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_collect",
    "group": "exchange_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_collect_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户userId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>报告类型</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_collect/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_collect.js",
    "groupTitle": "exchange_collect"
  },
  {
    "type": "post",
    "url": "/exchange_contenr/add",
    "title": "交流圈新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_contenr",
    "group": "exchange_contenr",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_center",
            "description": "<p>帖子内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_type",
            "description": "<p>帖子类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_userId",
            "description": "<p>发帖人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>['xxx.png','xxx.png','xxx.png','xxx.png','xxx.png','xxx.png']   交流圈图片</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_contenr/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_contenr.js",
    "groupTitle": "exchange_contenr"
  },
  {
    "type": "post",
    "url": "/exchange_contenr/delete",
    "title": "交流圈删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_contenr",
    "group": "exchange_contenr",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_id",
            "description": "<p>帖子ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_contenr/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_contenr.js",
    "groupTitle": "exchange_contenr"
  },
  {
    "type": "post",
    "url": "/exchange_contenr/queryList",
    "title": "交流圈查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_contenr",
    "group": "exchange_contenr",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>['xxx.png','xxx.png','xxx.png','xxx.png','xxx.png','xxx.png']   交流圈图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_user_info",
            "description": "<p>用户信息 --返回数据为用户信息表</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give",
            "description": "<p>是否已点赞 未点赞未null</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give_length",
            "description": "<p>点赞人数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content_comment_length",
            "description": "<p>评论内容长度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "is_exchange_follow",
            "description": "<p>自己是否关注本用户 true关注 false未关注</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_contenr/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_contenr.js",
    "groupTitle": "exchange_contenr"
  },
  {
    "type": "post",
    "url": "/exchange_contenr/query",
    "title": "交流圈查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_contenr",
    "group": "exchange_contenr",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_id",
            "description": "<p>帖子ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_center",
            "description": "<p>帖子内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_type",
            "description": "<p>帖子类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_userId",
            "description": "<p>发帖人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>['xxx.png','xxx.png','xxx.png','xxx.png','xxx.png','xxx.png']   交流圈图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_user_info",
            "description": "<p>用户信息 --返回数据为用户信息表</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give",
            "description": "<p>是否自己已点赞 未点赞未null</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content_comment",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give_length",
            "description": "<p>点赞人数</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "is_exchange_follow",
            "description": "<p>自己是否关注本用户 true关注 false未关注</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_card_id\":\"帖子ID\"\n        \"exchange_card_center\":\"帖子内容\"\n        \"exchange_card_type\":\"帖子类型\"\n        \"exchange_card_userId\":\"发帖人\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"exchange_img\":\"['xxx.png','xxx.png','xxx.png','xxx.png','xxx.png','xxx.png']   交流圈图片\"\n        \"sphy_user_info\":\"用户信息 --返回数据为用户信息表\"\n        \"exchange_give\":\"是否自己已点赞 未点赞未null\"\n        \"exchange_content_comment\":\"评论内容\"\n        \"exchange_give_length\":\"点赞人数\"\n        \"is_exchange_follow\":\"自己是否关注本用户 true关注 false未关注\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_contenr/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_contenr.js",
    "groupTitle": "exchange_contenr"
  },
  {
    "type": "post",
    "url": "/exchange_contenr/update",
    "title": "交流圈更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_contenr",
    "group": "exchange_contenr",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_id",
            "description": "<p>帖子ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_center",
            "description": "<p>帖子内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_type",
            "description": "<p>帖子类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_card_userId",
            "description": "<p>发帖人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_contenr/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_contenr.js",
    "groupTitle": "exchange_contenr"
  },
  {
    "type": "post",
    "url": "/exchange_content_comment/add",
    "title": "交流圈互相评论表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_content_comment",
    "group": "exchange_content_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_commentator_id",
            "description": "<p>评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_be_commentator_id",
            "description": "<p>被评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content_id",
            "description": "<p>评论文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type",
            "description": "<p>评论类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_content_comment/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_content_comment.js",
    "groupTitle": "exchange_content_comment"
  },
  {
    "type": "post",
    "url": "/exchange_content_comment/delete",
    "title": "交流圈互相评论表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_content_comment",
    "group": "exchange_content_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_comment_id",
            "description": "<p>帖子评论ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_content_comment/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_content_comment.js",
    "groupTitle": "exchange_content_comment"
  },
  {
    "type": "post",
    "url": "/exchange_content_comment/queryList",
    "title": "交流圈互相评论表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_content_comment",
    "group": "exchange_content_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_content_comment/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_content_comment.js",
    "groupTitle": "exchange_content_comment"
  },
  {
    "type": "post",
    "url": "/exchange_content_comment/query",
    "title": "交流圈互相评论表查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_content_comment",
    "group": "exchange_content_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_comment_id",
            "description": "<p>帖子评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_commentator_id",
            "description": "<p>评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_be_commentator_id",
            "description": "<p>被评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content_id",
            "description": "<p>评论文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type",
            "description": "<p>评论类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_comment_id\":\"帖子评论ID\"\n        \"exchange_commentator_id\":\"评论人ID\"\n        \"exchange_be_commentator_id\":\"被评论人ID\"\n        \"exchange_content_id\":\"评论文章ID\"\n        \"exchange_content\":\"评论内容\"\n        \"exchange_img\":\"评论图片\"\n        \"exchange_type\":\"评论类型\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_content_comment/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_content_comment.js",
    "groupTitle": "exchange_content_comment"
  },
  {
    "type": "post",
    "url": "/exchange_content_comment/update",
    "title": "交流圈互相评论表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_content_comment",
    "group": "exchange_content_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_comment_id",
            "description": "<p>帖子评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_commentator_id",
            "description": "<p>评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_be_commentator_id",
            "description": "<p>被评论人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content_id",
            "description": "<p>评论文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type",
            "description": "<p>评论类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_content_comment/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_content_comment.js",
    "groupTitle": "exchange_content_comment"
  },
  {
    "type": "post",
    "url": "/exchange_follow/add",
    "title": "用户关注",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_follow",
    "group": "exchange_follow",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "attention",
            "description": "<p>关注 -- 自己的ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_attention",
            "description": "<p>被关注 -别人的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_follow/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_follow.js",
    "groupTitle": "exchange_follow"
  },
  {
    "type": "post",
    "url": "/exchange_follow/delete",
    "title": "取消关注",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_follow",
    "group": "exchange_follow",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>交流圈关注人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_attention",
            "description": "<p>取消关注人ID (exchange_id跟be_attention有一个就行)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_follow/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_follow.js",
    "groupTitle": "exchange_follow"
  },
  {
    "type": "post",
    "url": "/exchange_follow/queryList",
    "title": "交流圈用户互关信息查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_follow",
    "group": "exchange_follow",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_follow/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_follow.js",
    "groupTitle": "exchange_follow"
  },
  {
    "type": "post",
    "url": "/exchange_follow/query",
    "title": "交流圈用户互关信息查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_follow",
    "group": "exchange_follow",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>交流圈关注人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "attention",
            "description": "<p>关注 -- 自己的ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_attention",
            "description": "<p>被关注 -别人的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_id\":\"交流圈关注人\"\n        \"attention\":\"关注 -- 自己的ID\"\n        \"be_attention\":\"被关注 -别人的id\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_follow/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_follow.js",
    "groupTitle": "exchange_follow"
  },
  {
    "type": "post",
    "url": "/exchange_follow/update",
    "title": "交流圈用户互关信息更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_follow",
    "group": "exchange_follow",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>交流圈关注人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "attention",
            "description": "<p>关注 -- 自己的ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_attention",
            "description": "<p>被关注 -别人的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_follow/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_follow.js",
    "groupTitle": "exchange_follow"
  },
  {
    "type": "post",
    "url": "/exchange_give/add",
    "title": "交流圈点赞",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_give",
    "group": "exchange_give",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>交流圈ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_give/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_give.js",
    "groupTitle": "exchange_give"
  },
  {
    "type": "post",
    "url": "/exchange_give/delete",
    "title": "取消点赞",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_give",
    "group": "exchange_give",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give_id",
            "description": "<p>点赞id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_give/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_give.js",
    "groupTitle": "exchange_give"
  },
  {
    "type": "post",
    "url": "/exchange_give/queryList",
    "title": "交流圈点赞查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_give",
    "group": "exchange_give",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_give/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_give.js",
    "groupTitle": "exchange_give"
  },
  {
    "type": "post",
    "url": "/exchange_give/query",
    "title": "交流圈点赞查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_give",
    "group": "exchange_give",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give_id",
            "description": "<p>点赞id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>交流圈ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_give_id\":\"点赞id\"\n        \"exchange_conter_id\":\"交流圈ID\"\n        \"user_id\":\"用户ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_give/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_give.js",
    "groupTitle": "exchange_give"
  },
  {
    "type": "post",
    "url": "/exchange_give/update",
    "title": "交流圈点赞更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_give",
    "group": "exchange_give",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_give_id",
            "description": "<p>点赞id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_conter_id",
            "description": "<p>交流圈ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_give/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_give.js",
    "groupTitle": "exchange_give"
  },
  {
    "type": "post",
    "url": "/exchange_img/add",
    "title": "上传交流圈跟交流圈图片关联接口新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_img",
    "group": "exchange_img",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>帖子ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>帖子图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_img/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_img.js",
    "groupTitle": "exchange_img"
  },
  {
    "type": "post",
    "url": "/exchange_img/delete",
    "title": "上传交流圈跟交流圈图片关联接口删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_img",
    "group": "exchange_img",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_img/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_img.js",
    "groupTitle": "exchange_img"
  },
  {
    "type": "post",
    "url": "/exchange_img/queryList",
    "title": "上传交流圈跟交流圈图片关联接口查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_img",
    "group": "exchange_img",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_img/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_img.js",
    "groupTitle": "exchange_img"
  },
  {
    "type": "post",
    "url": "/exchange_img/query",
    "title": "上传交流圈跟交流圈图片关联接口查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_img",
    "group": "exchange_img",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>帖子ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>帖子图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_img_id\":\"\"\n        \"exchange_id\":\"帖子ID\"\n        \"exchange_img\":\"帖子图片地址\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_img/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_img.js",
    "groupTitle": "exchange_img"
  },
  {
    "type": "post",
    "url": "/exchange_img/update",
    "title": "上传交流圈跟交流圈图片关联接口更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_img",
    "group": "exchange_img",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>帖子ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_img",
            "description": "<p>帖子图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_img/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_img.js",
    "groupTitle": "exchange_img"
  },
  {
    "type": "post",
    "url": "/exchange_privateChat/add",
    "title": "用户私聊表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_privateChat",
    "group": "exchange_privateChat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_content",
            "description": "<p>私聊内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_type",
            "description": "<p>私聊类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_send_id",
            "description": "<p>发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_be_send_id",
            "description": "<p>被发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_privateChat/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_privateChat.js",
    "groupTitle": "exchange_privateChat"
  },
  {
    "type": "post",
    "url": "/exchange_privateChat/delete",
    "title": "用户私聊表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_privateChat",
    "group": "exchange_privateChat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_id",
            "description": "<p>私聊ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_privateChat/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_privateChat.js",
    "groupTitle": "exchange_privateChat"
  },
  {
    "type": "post",
    "url": "/exchange_privateChat/queryList",
    "title": "用户私聊表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_privateChat",
    "group": "exchange_privateChat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_privateChat/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_privateChat.js",
    "groupTitle": "exchange_privateChat"
  },
  {
    "type": "post",
    "url": "/exchange_privateChat/query",
    "title": "用户私聊表查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_privateChat",
    "group": "exchange_privateChat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_content",
            "description": "<p>私聊内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_id",
            "description": "<p>私聊ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_type",
            "description": "<p>私聊类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_send_id",
            "description": "<p>发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_be_send_id",
            "description": "<p>被发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_privateChat_content\":\"私聊内容\"\n        \"exchange_privateChat_id\":\"私聊ID\"\n        \"exchange_privateChat_type\":\"私聊类型\"\n        \"exchange_privateChat_send_id\":\"发送人ID\"\n        \"exchange_privateChat_be_send_id\":\"被发送人ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_privateChat/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_privateChat.js",
    "groupTitle": "exchange_privateChat"
  },
  {
    "type": "post",
    "url": "/exchange_privateChat/update",
    "title": "用户私聊表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_privateChat",
    "group": "exchange_privateChat",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_content",
            "description": "<p>私聊内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_id",
            "description": "<p>私聊ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_type",
            "description": "<p>私聊类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_send_id",
            "description": "<p>发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_privateChat_be_send_id",
            "description": "<p>被发送人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_privateChat/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_privateChat.js",
    "groupTitle": "exchange_privateChat"
  },
  {
    "type": "post",
    "url": "/exchange_type/add",
    "title": "交流圈栏目新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexchange_type",
    "group": "exchange_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_name",
            "description": "<p>栏目名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_msg",
            "description": "<p>栏目简介</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_type/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_type.js",
    "groupTitle": "exchange_type"
  },
  {
    "type": "post",
    "url": "/exchange_type/delete",
    "title": "交流圈栏目删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexchange_type",
    "group": "exchange_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>栏目ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_type/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_type.js",
    "groupTitle": "exchange_type"
  },
  {
    "type": "post",
    "url": "/exchange_type/queryList",
    "title": "交流圈栏目查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexchange_type",
    "group": "exchange_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exchange_type/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_type.js",
    "groupTitle": "exchange_type"
  },
  {
    "type": "post",
    "url": "/exchange_type/query",
    "title": "交流圈栏目查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexchange_type",
    "group": "exchange_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>栏目ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_name",
            "description": "<p>栏目名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_msg",
            "description": "<p>栏目简介</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exchange_type_id\":\"栏目ID\"\n        \"exchange_type_name\":\"栏目名字\"\n        \"exchange_type_msg\":\"栏目简介\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_type/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_type.js",
    "groupTitle": "exchange_type"
  },
  {
    "type": "post",
    "url": "/exchange_type/update",
    "title": "交流圈栏目更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexchange_type",
    "group": "exchange_type",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>栏目ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_name",
            "description": "<p>栏目名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_msg",
            "description": "<p>栏目简介</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exchange_type/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exchange_type.js",
    "groupTitle": "exchange_type"
  },
  {
    "type": "post",
    "url": "/exhibition/add",
    "title": "展馆信息新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addexhibition",
    "group": "exhibition",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_name",
            "description": "<p>展馆名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_address",
            "description": "<p>展馆地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lot",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lat",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_Img",
            "description": "<p>展馆图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exhibition/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exhibition.js",
    "groupTitle": "exhibition"
  },
  {
    "type": "post",
    "url": "/exhibition/delete",
    "title": "展馆信息删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deleteexhibition",
    "group": "exhibition",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_id",
            "description": "<p>展馆ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exhibition/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exhibition.js",
    "groupTitle": "exhibition"
  },
  {
    "type": "post",
    "url": "/exhibition/queryList",
    "title": "展馆信息查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListexhibition",
    "group": "exhibition",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/exhibition/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exhibition.js",
    "groupTitle": "exhibition"
  },
  {
    "type": "post",
    "url": "/exhibition/query",
    "title": "展馆信息查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryexhibition",
    "group": "exhibition",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_id",
            "description": "<p>展馆ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_name",
            "description": "<p>展馆名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_address",
            "description": "<p>展馆地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lot",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lat",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_Img",
            "description": "<p>展馆图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"exhibition_id\":\"展馆ID\"\n        \"exhibition_name\":\"展馆名字\"\n        \"exhibition_address\":\"展馆地址\"\n        \"exhibition_lot\":\"经度\"\n        \"exhibition_lat\":\"纬度\"\n        \"exhibition_Img\":\"展馆图片\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exhibition/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exhibition.js",
    "groupTitle": "exhibition"
  },
  {
    "type": "post",
    "url": "/exhibition/update",
    "title": "展馆信息更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updateexhibition",
    "group": "exhibition",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_id",
            "description": "<p>展馆ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_name",
            "description": "<p>展馆名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_address",
            "description": "<p>展馆地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lot",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_lat",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exhibition_Img",
            "description": "<p>展馆图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/exhibition/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/exhibition.js",
    "groupTitle": "exhibition"
  },
  {
    "type": "post",
    "url": "/note/add",
    "title": "笔记新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addnote",
    "group": "note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_name",
            "description": "<p>笔记标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_type",
            "description": "<p>笔记类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_content",
            "description": "<p>笔记内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_delete",
            "description": "<p>笔记是否存在云盘</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_user_id",
            "description": "<p>笔记记录人ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/note/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/note.js",
    "groupTitle": "note"
  },
  {
    "type": "post",
    "url": "/note/delete",
    "title": "笔记删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletenote",
    "group": "note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_id",
            "description": "<p>笔记ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/note/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/note.js",
    "groupTitle": "note"
  },
  {
    "type": "post",
    "url": "/note/queryList",
    "title": "笔记查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListnote",
    "group": "note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/note/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/note.js",
    "groupTitle": "note"
  },
  {
    "type": "post",
    "url": "/note/query",
    "title": "笔记查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querynote",
    "group": "note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_id",
            "description": "<p>笔记ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_name",
            "description": "<p>笔记标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_type",
            "description": "<p>笔记类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_content",
            "description": "<p>笔记内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_delete",
            "description": "<p>笔记是否存在云盘</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_user_id",
            "description": "<p>笔记记录人ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"note_id\":\"笔记ID\"\n        \"note_name\":\"笔记标题\"\n        \"note_type\":\"笔记类型\"\n        \"note_content\":\"笔记内容\"\n        \"note_delete\":\"笔记是否存在云盘\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"note_user_id\":\"笔记记录人ID\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/note/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/note.js",
    "groupTitle": "note"
  },
  {
    "type": "post",
    "url": "/note/update",
    "title": "笔记更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatenote",
    "group": "note",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_id",
            "description": "<p>笔记ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_name",
            "description": "<p>笔记标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_type",
            "description": "<p>笔记类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_content",
            "description": "<p>笔记内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_delete",
            "description": "<p>笔记是否存在云盘</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note_user_id",
            "description": "<p>笔记记录人ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/note/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/note.js",
    "groupTitle": "note"
  },
  {
    "type": "post",
    "url": "/report_collect/add",
    "title": "报告/会议收藏新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addreport_collect",
    "group": "report_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>收藏用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_collect/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_collect.js",
    "groupTitle": "report_collect"
  },
  {
    "type": "post",
    "url": "/report_collect/delete",
    "title": "报告/会议收藏删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletereport_collect",
    "group": "report_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_collect_id",
            "description": "<p>签约收藏ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_collect/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_collect.js",
    "groupTitle": "report_collect"
  },
  {
    "type": "post",
    "url": "/report_collect/queryList",
    "title": "报告/会议收藏查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListreport_collect",
    "group": "report_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_collect/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_collect.js",
    "groupTitle": "report_collect"
  },
  {
    "type": "post",
    "url": "/report_collect/query",
    "title": "报告/会议收藏查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryreport_collect",
    "group": "report_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_collect_id",
            "description": "<p>签约收藏ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>收藏用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"report_collect_id\":\"签约收藏ID\"\n        \"userId\":\"收藏用户ID\"\n        \"exchange_id\":\"报告ID\"\n        \"exchange_type_id\":\"报告类型ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_collect/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_collect.js",
    "groupTitle": "report_collect"
  },
  {
    "type": "post",
    "url": "/report_collect/update",
    "title": "报告/会议收藏更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatereport_collect",
    "group": "report_collect",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_collect_id",
            "description": "<p>签约收藏ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>收藏用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_collect/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_collect.js",
    "groupTitle": "report_collect"
  },
  {
    "type": "post",
    "url": "/report_comment/add",
    "title": "报告评论新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addreport_comment",
    "group": "report_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评论类型如（富文本/普通对话/组建等） 你发什么给你什么</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_userid",
            "description": "<p>被评论用户的UserID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_comment/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_comment.js",
    "groupTitle": "report_comment"
  },
  {
    "type": "post",
    "url": "/report_comment/delete",
    "title": "报告评论删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletereport_comment",
    "group": "report_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_comment_id",
            "description": "<p>报告评论ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_comment/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_comment.js",
    "groupTitle": "report_comment"
  },
  {
    "type": "post",
    "url": "/report_comment/queryList",
    "title": "报告评论查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListreport_comment",
    "group": "report_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_comment/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_comment.js",
    "groupTitle": "report_comment"
  },
  {
    "type": "post",
    "url": "/report_comment/query",
    "title": "报告评论查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryreport_comment",
    "group": "report_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_comment_id",
            "description": "<p>报告评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评论类型如（富文本/普通对话/组建等） 你发什么给你什么</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_userid",
            "description": "<p>被评论用户的UserID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"report_comment_id\":\"报告评论ID\"\n        \"content\":\"评论内容\"\n        \"userId\":\"用户ID\"\n        \"exchange_id\":\"报告ID\"\n        \"exchange_type_id\":\"报告类型ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"type\":\"评论类型如（富文本/普通对话/组建等） 你发什么给你什么\"\n        \"be_userid\":\"被评论用户的UserID\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_comment/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_comment.js",
    "groupTitle": "report_comment"
  },
  {
    "type": "post",
    "url": "/report_comment/update",
    "title": "报告评论更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatereport_comment",
    "group": "report_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_comment_id",
            "description": "<p>报告评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userId",
            "description": "<p>用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "exchange_type_id",
            "description": "<p>报告类型ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>评论类型如（富文本/普通对话/组建等） 你发什么给你什么</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_userid",
            "description": "<p>被评论用户的UserID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_comment/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_comment.js",
    "groupTitle": "report_comment"
  },
  {
    "type": "post",
    "url": "/report_poster_user/add",
    "title": "海报集作者新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addreport_poster_user",
    "group": "report_poster_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_user_id",
            "description": "<p>海报集作者专属ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报集ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>作者ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_type",
            "description": "<p>1作者  2协作</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_poster_user/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster_user.js",
    "groupTitle": "report_poster_user"
  },
  {
    "type": "post",
    "url": "/report_poster_user/delete",
    "title": "海报集作者删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletereport_poster_user",
    "group": "report_poster_user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster_user/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster_user.js",
    "groupTitle": "report_poster_user"
  },
  {
    "type": "post",
    "url": "/report_poster_user/queryList",
    "title": "海报集作者查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListreport_poster_user",
    "group": "report_poster_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_poster_user/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster_user.js",
    "groupTitle": "report_poster_user"
  },
  {
    "type": "post",
    "url": "/report_poster_user/query",
    "title": "海报集作者查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryreport_poster_user",
    "group": "report_poster_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_user_id",
            "description": "<p>海报集作者专属ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报集ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>作者ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_type",
            "description": "<p>1作者  2协作</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"report_poster_user_id\":\"海报集作者专属ID\"\n        \"report_poster_id\":\"海报集ID\"\n        \"user_id\":\"作者ID\"\n        \"report_poster_type\":\"1作者  2协作\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster_user/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster_user.js",
    "groupTitle": "report_poster_user"
  },
  {
    "type": "post",
    "url": "/report_poster_user/update",
    "title": "海报集作者更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatereport_poster_user",
    "group": "report_poster_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_user_id",
            "description": "<p>海报集作者专属ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报集ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>作者ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_type",
            "description": "<p>1作者  2协作</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster_user/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster_user.js",
    "groupTitle": "report_poster_user"
  },
  {
    "type": "post",
    "url": "/report_poster/add",
    "title": "海报集新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addreport_poster",
    "group": "report_poster",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_name",
            "description": "<p>海报标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_number",
            "description": "<p>海报编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_img",
            "description": "<p>海报图片</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_poster/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster.js",
    "groupTitle": "report_poster"
  },
  {
    "type": "post",
    "url": "/report_poster/delete",
    "title": "海报集删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletereport_poster",
    "group": "report_poster",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster.js",
    "groupTitle": "report_poster"
  },
  {
    "type": "post",
    "url": "/report_poster/queryList",
    "title": "海报集查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListreport_poster",
    "group": "report_poster",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial",
            "description": "<p>报告 --返回数据为报告信息表</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_users",
            "description": "<p>海报关联人 --返回数据为海报作者表</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_users.user_id",
            "description": "<p>海报用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_users.report_poster_type",
            "description": "<p>1作者 2协作</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/report_poster/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster.js",
    "groupTitle": "report_poster"
  },
  {
    "type": "post",
    "url": "/report_poster/query",
    "title": "海报集查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryreport_poster",
    "group": "report_poster",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_name",
            "description": "<p>海报标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_number",
            "description": "<p>海报编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_img",
            "description": "<p>海报图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"report_poster_id\":\"海报ID\"\n        \"serial_id\":\"报告ID\"\n        \"report_poster_name\":\"海报标题\"\n        \"report_poster_number\":\"海报编号\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"report_poster_img\":\"海报图片\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster.js",
    "groupTitle": "report_poster"
  },
  {
    "type": "post",
    "url": "/report_poster/update",
    "title": "海报集更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatereport_poster",
    "group": "report_poster",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_id",
            "description": "<p>海报ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_name",
            "description": "<p>海报标题</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_number",
            "description": "<p>海报编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "report_poster_img",
            "description": "<p>海报图片</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/report_poster/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/report_poster.js",
    "groupTitle": "report_poster"
  },
  {
    "type": "post",
    "url": "/sphy_banner/add",
    "title": "banner轮播图新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_banner",
    "group": "sphy_banner",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "banner",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "bannerType",
            "description": "<p>banner类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_banner/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_banner.js",
    "groupTitle": "sphy_banner"
  },
  {
    "type": "post",
    "url": "/sphy_banner/delete",
    "title": "banner轮播图删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_banner",
    "group": "sphy_banner",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_banner/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_banner.js",
    "groupTitle": "sphy_banner"
  },
  {
    "type": "post",
    "url": "/sphy_banner/queryList",
    "title": "banner轮播图查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_banner",
    "group": "sphy_banner",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_banner/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_banner.js",
    "groupTitle": "sphy_banner"
  },
  {
    "type": "post",
    "url": "/sphy_banner/query",
    "title": "banner轮播图查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_banner",
    "group": "sphy_banner",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "banner",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "bannerType",
            "description": "<p>banner类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"amend_id\":\"操作用户\"\n        \"id\":\"\"\n        \"banner\":\"\"\n        \"bannerType\":\"banner类型\"\n        \"url\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_banner/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_banner.js",
    "groupTitle": "sphy_banner"
  },
  {
    "type": "post",
    "url": "/sphy_banner/update",
    "title": "banner轮播图更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_banner",
    "group": "sphy_banner",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "banner",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "bannerType",
            "description": "<p>banner类型</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_banner/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_banner.js",
    "groupTitle": "sphy_banner"
  },
  {
    "type": "post",
    "url": "/sphy_script/add",
    "title": "系统图片等配置新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_script",
    "group": "sphy_script",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>文字内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "script_type",
            "description": "<p>type汉字类型 如底部按钮 中部简介</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_script/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_script.js",
    "groupTitle": "sphy_script"
  },
  {
    "type": "post",
    "url": "/sphy_script/delete",
    "title": "系统图片等配置删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_script",
    "group": "sphy_script",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_script/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_script.js",
    "groupTitle": "sphy_script"
  },
  {
    "type": "post",
    "url": "/sphy_script/queryList",
    "title": "系统图片等配置查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_script",
    "group": "sphy_script",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_script/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_script.js",
    "groupTitle": "sphy_script"
  },
  {
    "type": "post",
    "url": "/sphy_script/query",
    "title": "系统图片等配置查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_script",
    "group": "sphy_script",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>文字内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "script_type",
            "description": "<p>type汉字类型 如底部按钮 中部简介</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"id\":\"\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"sort\":\"排序\"\n        \"amend_id\":\"操作用户\"\n        \"img\":\"图片\"\n        \"url\":\"链接地址\"\n        \"name\":\"文字内容\"\n        \"script_type\":\"type汉字类型 如底部按钮 中部简介 \"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_script/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_script.js",
    "groupTitle": "sphy_script"
  },
  {
    "type": "post",
    "url": "/sphy_script/update",
    "title": "系统图片等配置更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_script",
    "group": "sphy_script",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "amend_id",
            "description": "<p>操作用户</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>文字内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "script_type",
            "description": "<p>type汉字类型 如底部按钮 中部简介</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_script/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_script.js",
    "groupTitle": "sphy_script"
  },
  {
    "type": "post",
    "url": "/sphy_serial_comment/add",
    "title": "报告评论-弃用新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_serial_comment",
    "group": "sphy_serial_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_comment_id",
            "description": "<p>报告评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>评论人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_user_id",
            "description": "<p>被评论用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_comment/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_comment.js",
    "groupTitle": "sphy_serial_comment"
  },
  {
    "type": "post",
    "url": "/sphy_serial_comment/delete",
    "title": "报告评论-弃用删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_serial_comment",
    "group": "sphy_serial_comment",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_comment/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_comment.js",
    "groupTitle": "sphy_serial_comment"
  },
  {
    "type": "post",
    "url": "/sphy_serial_comment/queryList",
    "title": "报告评论-弃用查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_serial_comment",
    "group": "sphy_serial_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_comment/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_comment.js",
    "groupTitle": "sphy_serial_comment"
  },
  {
    "type": "post",
    "url": "/sphy_serial_comment/query",
    "title": "报告评论-弃用查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_serial_comment",
    "group": "sphy_serial_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_comment_id",
            "description": "<p>报告评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>评论人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_user_id",
            "description": "<p>被评论用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"sphy_serial_comment_id\":\"报告评论ID\"\n        \"user_id\":\"评论人\"\n        \"content\":\"评论内容\"\n        \"img\":\"评论图片\"\n        \"be_user_id\":\"被评论用户ID\"\n        \"serial_id\":\"报告ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"sort\":\"排序\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_comment/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_comment.js",
    "groupTitle": "sphy_serial_comment"
  },
  {
    "type": "post",
    "url": "/sphy_serial_comment/update",
    "title": "报告评论-弃用更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_serial_comment",
    "group": "sphy_serial_comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_comment_id",
            "description": "<p>报告评论ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>评论人</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>评论图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "be_user_id",
            "description": "<p>被评论用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_comment/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_comment.js",
    "groupTitle": "sphy_serial_comment"
  },
  {
    "type": "post",
    "url": "/sphy_serial_user/add",
    "title": "用户订阅报告新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_serial_user",
    "group": "sphy_serial_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>订阅ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_user/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_user.js",
    "groupTitle": "sphy_serial_user"
  },
  {
    "type": "post",
    "url": "/sphy_serial_user/delete",
    "title": "用户订阅报告删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_serial_user",
    "group": "sphy_serial_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_id",
            "description": "<p>用户订阅的报告ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_user/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_user.js",
    "groupTitle": "sphy_serial_user"
  },
  {
    "type": "post",
    "url": "/sphy_serial_user/queryList",
    "title": "用户订阅报告查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_serial_user",
    "group": "sphy_serial_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_user/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_user.js",
    "groupTitle": "sphy_serial_user"
  },
  {
    "type": "post",
    "url": "/sphy_serial_user/query",
    "title": "用户订阅报告查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_serial_user",
    "group": "sphy_serial_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_id",
            "description": "<p>用户订阅的报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>订阅ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"sphy_serial_id\":\"用户订阅的报告ID\"\n        \"user_id\":\"订阅ID\"\n        \"serial_id\":\"报告ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"sort\":\"排序\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_user/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_user.js",
    "groupTitle": "sphy_serial_user"
  },
  {
    "type": "post",
    "url": "/sphy_serial_user/update",
    "title": "用户订阅报告更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_serial_user",
    "group": "sphy_serial_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sphy_serial_id",
            "description": "<p>用户订阅的报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>订阅ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial_user/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial_user.js",
    "groupTitle": "sphy_serial_user"
  },
  {
    "type": "post",
    "url": "/sphy_serial/add",
    "title": "报告表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_serial",
    "group": "sphy_serial",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_serial",
            "description": "<p>报告编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_name",
            "description": "<p>报告名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "keynote_id",
            "description": "<p>主讲人id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>报告所在房间 --会议ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "abstract_id",
            "description": "<p>摘要ID --删除</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pdf_url",
            "description": "<p>pdf地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial.js",
    "groupTitle": "sphy_serial"
  },
  {
    "type": "post",
    "url": "/sphy_serial/delete",
    "title": "报告表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_serial",
    "group": "sphy_serial",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial.js",
    "groupTitle": "sphy_serial"
  },
  {
    "type": "post",
    "url": "/sphy_serial/queryList",
    "title": "报告表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_serial",
    "group": "sphy_serial",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial.js",
    "groupTitle": "sphy_serial"
  },
  {
    "type": "post",
    "url": "/sphy_serial/query",
    "title": "报告表查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_serial",
    "group": "sphy_serial",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_serial",
            "description": "<p>报告编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_name",
            "description": "<p>报告名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "keynote_id",
            "description": "<p>主讲人id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>报告所在房间 --会议ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "abstract_id",
            "description": "<p>摘要ID --删除</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pdf_url",
            "description": "<p>pdf地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"serial_id\":\"\"\n        \"serial_serial\":\"报告编号\"\n        \"serial_name\":\"报告名称\"\n        \"keynote_id\":\"主讲人id\"\n        \"in_then_room\":\"报告所在房间 --会议ID\"\n        \"abstract_id\":\"摘要ID --删除\"\n        \"pdf_url\":\"pdf地址\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"sort\":\"排序\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial.js",
    "groupTitle": "sphy_serial"
  },
  {
    "type": "post",
    "url": "/sphy_serial/update",
    "title": "报告表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_serial",
    "group": "sphy_serial",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_serial",
            "description": "<p>报告编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_name",
            "description": "<p>报告名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "keynote_id",
            "description": "<p>主讲人id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "in_then_room",
            "description": "<p>报告所在房间 --会议ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "abstract_id",
            "description": "<p>摘要ID --删除</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pdf_url",
            "description": "<p>pdf地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_serial/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_serial.js",
    "groupTitle": "sphy_serial"
  },
  {
    "type": "post",
    "url": "/sphy_summaries/add",
    "title": "用户报告摘要新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_summaries",
    "group": "sphy_summaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>报告作者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>报告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>报告的额外图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_summaries/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_summaries.js",
    "groupTitle": "sphy_summaries"
  },
  {
    "type": "post",
    "url": "/sphy_summaries/delete",
    "title": "用户报告摘要删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_summaries",
    "group": "sphy_summaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "summaries_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_summaries/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_summaries.js",
    "groupTitle": "sphy_summaries"
  },
  {
    "type": "post",
    "url": "/sphy_summaries/queryList",
    "title": "用户报告摘要查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_summaries",
    "group": "sphy_summaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_summaries/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_summaries.js",
    "groupTitle": "sphy_summaries"
  },
  {
    "type": "post",
    "url": "/sphy_summaries/query",
    "title": "用户报告摘要查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_summaries",
    "group": "sphy_summaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "summaries_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>报告作者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>报告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>报告的额外图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"summaries_id\":\"\"\n        \"user_id\":\"报告作者\"\n        \"content\":\"报告内容\"\n        \"img\":\"报告的额外图片\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"sort\":\"排序\"\n        \"serial_id\":\"报告ID\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_summaries/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_summaries.js",
    "groupTitle": "sphy_summaries"
  },
  {
    "type": "post",
    "url": "/sphy_summaries/update",
    "title": "用户报告摘要更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_summaries",
    "group": "sphy_summaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "summaries_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>报告作者</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>报告内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "img",
            "description": "<p>报告的额外图片</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_summaries/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_summaries.js",
    "groupTitle": "sphy_summaries"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/add",
    "title": "用户表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsphy_user_info",
    "group": "sphy_user_info",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "head",
            "description": "<p>头像</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mail",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "surname",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "county",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>城</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "town",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "district",
            "description": "<p>区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>完整地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>国家</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_info.js",
    "groupTitle": "sphy_user_info"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/delete",
    "title": "用户表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesphy_user_info",
    "group": "sphy_user_info",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_info_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_info.js",
    "groupTitle": "sphy_user_info"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/queryList",
    "title": "用户表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsphy_user_info",
    "group": "sphy_user_info",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_info.js",
    "groupTitle": "sphy_user_info"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/query",
    "title": "作者集",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysphy_user_info",
    "group": "sphy_user_info",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_info_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "head",
            "description": "<p>头像</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mail",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "surname",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "county",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>城</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "town",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "district",
            "description": "<p>区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>完整地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>国家</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"user_info_id\":\"\"\n        \"head\":\"头像\"\n        \"mail\":\"邮箱\"\n        \"name\":\"名\"\n        \"surname\":\"姓\"\n        \"unit\":\"单位\"\n        \"county\":\"单位\"\n        \"city\":\"城\"\n        \"town\":\"市\"\n        \"district\":\"区\"\n        \"address\":\"完整地址\"\n        \"state\":\"国家\"\n        \"password\":\"密码\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n        \"role_id\":\"角色ID\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_info.js",
    "groupTitle": "sphy_user_info"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/update",
    "title": "用户表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesphy_user_info",
    "group": "sphy_user_info",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_info_id",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "head",
            "description": "<p>头像</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mail",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "surname",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "county",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>城</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "town",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "district",
            "description": "<p>区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>完整地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>国家</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_info.js",
    "groupTitle": "sphy_user_info"
  },
  {
    "type": "post",
    "url": "/system_memu_role/add",
    "title": "菜单关联角色ID新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsystem_memu_role",
    "group": "system_memu_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_role_id",
            "description": "<p>菜单关联角色id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu_role/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_role.js",
    "groupTitle": "system_memu_role"
  },
  {
    "type": "post",
    "url": "/system_memu_role/delete",
    "title": "菜单关联角色ID删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesystem_memu_role",
    "group": "system_memu_role",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_role/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_role.js",
    "groupTitle": "system_memu_role"
  },
  {
    "type": "post",
    "url": "/system_memu_role/queryList",
    "title": "菜单关联角色ID查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsystem_memu_role",
    "group": "system_memu_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu_role/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_role.js",
    "groupTitle": "system_memu_role"
  },
  {
    "type": "post",
    "url": "/system_memu_role/query",
    "title": "菜单关联角色ID查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysystem_memu_role",
    "group": "system_memu_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_role_id",
            "description": "<p>菜单关联角色id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"system_memu_role_id\":\"菜单关联角色id\"\n        \"role_id\":\"角色ID\"\n        \"memu_id\":\"菜单ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_role/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_role.js",
    "groupTitle": "system_memu_role"
  },
  {
    "type": "post",
    "url": "/system_memu_role/update",
    "title": "菜单关联角色ID更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesystem_memu_role",
    "group": "system_memu_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_role_id",
            "description": "<p>菜单关联角色id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_role/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_role.js",
    "groupTitle": "system_memu_role"
  },
  {
    "type": "post",
    "url": "/system_memu_select/add",
    "title": "用户角色菜单ID新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsystem_memu_select",
    "group": "system_memu_select",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_value",
            "description": "<p>菜单按钮内容 -标识</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_state",
            "description": "<p>1启用 2禁用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>用户菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu_select/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_select.js",
    "groupTitle": "system_memu_select"
  },
  {
    "type": "post",
    "url": "/system_memu_select/delete",
    "title": "用户角色菜单ID删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesystem_memu_select",
    "group": "system_memu_select",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_id",
            "description": "<p>用户菜单选项ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_select/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_select.js",
    "groupTitle": "system_memu_select"
  },
  {
    "type": "post",
    "url": "/system_memu_select/queryList",
    "title": "用户角色菜单ID查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsystem_memu_select",
    "group": "system_memu_select",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu_select/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_select.js",
    "groupTitle": "system_memu_select"
  },
  {
    "type": "post",
    "url": "/system_memu_select/query",
    "title": "用户角色菜单ID查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysystem_memu_select",
    "group": "system_memu_select",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_id",
            "description": "<p>用户菜单选项ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_value",
            "description": "<p>菜单按钮内容 -标识</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_state",
            "description": "<p>1启用 2禁用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>用户菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"system_memu_select_id\":\"用户菜单选项ID\"\n        \"system_memu_select_value\":\"菜单按钮内容 -标识\"\n        \"system_memu_state\":\"1启用 2禁用\"\n        \"system_memu_id\":\"用户菜单ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_select/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_select.js",
    "groupTitle": "system_memu_select"
  },
  {
    "type": "post",
    "url": "/system_memu_select/update",
    "title": "用户角色菜单ID更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesystem_memu_select",
    "group": "system_memu_select",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_id",
            "description": "<p>用户菜单选项ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_select_value",
            "description": "<p>菜单按钮内容 -标识</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_state",
            "description": "<p>1启用 2禁用</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>用户菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu_select/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu_select.js",
    "groupTitle": "system_memu_select"
  },
  {
    "type": "post",
    "url": "/system_memu/add",
    "title": "菜单表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsystem_memu",
    "group": "system_memu",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_router",
            "description": "<p>菜单路由</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_name",
            "description": "<p>菜单名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_superior_id",
            "description": "<p>上级菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu.js",
    "groupTitle": "system_memu"
  },
  {
    "type": "post",
    "url": "/system_memu/delete",
    "title": "菜单表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesystem_memu",
    "group": "system_memu",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu.js",
    "groupTitle": "system_memu"
  },
  {
    "type": "post",
    "url": "/system_memu/queryList",
    "title": "菜单表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsystem_memu",
    "group": "system_memu",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_memu/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu.js",
    "groupTitle": "system_memu"
  },
  {
    "type": "post",
    "url": "/system_memu/query",
    "title": "菜单表查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysystem_memu",
    "group": "system_memu",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_router",
            "description": "<p>菜单路由</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_name",
            "description": "<p>菜单名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_superior_id",
            "description": "<p>上级菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"system_memu_id\":\"菜单ID\"\n        \"system_memu_router\":\"菜单路由\"\n        \"system_memu_name\":\"菜单名字\"\n        \"system_memu_superior_id\":\"上级菜单ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu.js",
    "groupTitle": "system_memu"
  },
  {
    "type": "post",
    "url": "/system_memu/update",
    "title": "菜单表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesystem_memu",
    "group": "system_memu",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_id",
            "description": "<p>菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_router",
            "description": "<p>菜单路由</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_name",
            "description": "<p>菜单名字</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_memu_superior_id",
            "description": "<p>上级菜单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_memu/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_memu.js",
    "groupTitle": "system_memu"
  },
  {
    "type": "post",
    "url": "/system_role_user/add",
    "title": "用户关联角色新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsystem_role_user",
    "group": "system_role_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_user_role_id",
            "description": "<p>用户角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_role_user/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role_user.js",
    "groupTitle": "system_role_user"
  },
  {
    "type": "post",
    "url": "/system_role_user/delete",
    "title": "用户关联角色删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesystem_role_user",
    "group": "system_role_user",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role_user/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role_user.js",
    "groupTitle": "system_role_user"
  },
  {
    "type": "post",
    "url": "/system_role_user/queryList",
    "title": "用户关联角色查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsystem_role_user",
    "group": "system_role_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_role_user/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role_user.js",
    "groupTitle": "system_role_user"
  },
  {
    "type": "post",
    "url": "/system_role_user/query",
    "title": "用户关联角色查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysystem_role_user",
    "group": "system_role_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_user_role_id",
            "description": "<p>用户角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"system_user_role_id\":\"用户角色ID\"\n        \"user_id\":\"用户id\"\n        \"role_id\":\"角色ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role_user/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role_user.js",
    "groupTitle": "system_role_user"
  },
  {
    "type": "post",
    "url": "/system_role_user/update",
    "title": "用户关联角色更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesystem_role_user",
    "group": "system_role_user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_user_role_id",
            "description": "<p>用户角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role_user/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role_user.js",
    "groupTitle": "system_role_user"
  },
  {
    "type": "post",
    "url": "/system_role/add",
    "title": "角色表新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addsystem_role",
    "group": "system_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_name",
            "description": "<p>角色名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_describe",
            "description": "<p>角色描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_role/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role.js",
    "groupTitle": "system_role"
  },
  {
    "type": "post",
    "url": "/system_role/delete",
    "title": "角色表删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletesystem_role",
    "group": "system_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_id",
            "description": "<p>角色ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role.js",
    "groupTitle": "system_role"
  },
  {
    "type": "post",
    "url": "/system_role/queryList",
    "title": "角色表查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListsystem_role",
    "group": "system_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/system_role/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role.js",
    "groupTitle": "system_role"
  },
  {
    "type": "post",
    "url": "/system_role/query",
    "title": "角色表查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querysystem_role",
    "group": "system_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_name",
            "description": "<p>角色名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_describe",
            "description": "<p>角色描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"system_role_id\":\"角色ID\"\n        \"system_role_name\":\"角色名称\"\n        \"system_role_describe\":\"角色描述\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role.js",
    "groupTitle": "system_role"
  },
  {
    "type": "post",
    "url": "/system_role/update",
    "title": "角色表更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatesystem_role",
    "group": "system_role",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_id",
            "description": "<p>角色ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_name",
            "description": "<p>角色名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "system_role_describe",
            "description": "<p>角色描述</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/system_role/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/system_role.js",
    "groupTitle": "system_role"
  },
  {
    "type": "post",
    "url": "/theme/add",
    "title": "主题新增",
    "description": "<p>&quot;&quot;</p>",
    "name": "addtheme",
    "group": "theme",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>主题名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serialName",
            "description": "<p>主题编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/theme/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/theme.js",
    "groupTitle": "theme"
  },
  {
    "type": "post",
    "url": "/theme/delete",
    "title": "主题删除",
    "description": "<p>&quot;&quot;</p>",
    "name": "deletetheme",
    "group": "theme",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  {\n    status:200,\n    data:\"删除成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/theme/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/theme.js",
    "groupTitle": "theme"
  },
  {
    "type": "post",
    "url": "/theme/queryList",
    "title": "主题查询列表",
    "description": "<p>&quot;&quot;</p>",
    "name": "queryListtheme",
    "group": "theme",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": "<p>undefined</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/theme/queryList"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/theme.js",
    "groupTitle": "theme"
  },
  {
    "type": "post",
    "url": "/theme/query",
    "title": "主题查询详情",
    "description": "<p>&quot;&quot;</p>",
    "name": "querytheme",
    "group": "theme",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>主题名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serialName",
            "description": "<p>主题编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "page",
            "description": "<p>分页，如果不需要可不发</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>分页数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "对应的字段\n  {\n    \n        \"theme_id\":\"主题ID\"\n        \"name\":\"主题名称\"\n        \"serialName\":\"主题编号\"\n        \"serial_id\":\"报告ID\"\n        \"create_time\":\"\"\n        \"update_time\":\"\"\n        \"del_flag\":\"\"\n        \"remark\":\"\"\n        \"remark1\":\"\"\n        \"sort\":\"排序\"\n        \"language\":\"中文(Chinese) 英文(English)\"\n   }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/theme/query"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/theme.js",
    "groupTitle": "theme"
  },
  {
    "type": "post",
    "url": "/theme/update",
    "title": "主题更新",
    "description": "<p>&quot;&quot;</p>",
    "name": "updatetheme",
    "group": "theme",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "theme_id",
            "description": "<p>主题ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>主题名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serialName",
            "description": "<p>主题编号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "serial_id",
            "description": "<p>报告ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "create_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "update_time",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "del_flag",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "remark1",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "language",
            "description": "<p>中文(Chinese) 英文(English)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "code",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n   status:200,\n   data:\"修改成功\"\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/theme/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/theme.js",
    "groupTitle": "theme"
  },
  {
    "type": "post",
    "url": "/filesUpload/add",
    "title": "文件上传",
    "description": "<p>&quot;&quot;</p>",
    "name": "文件上传",
    "group": "文件上传",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "file",
            "description": "<p>文件file</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/filesUpload/add"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/filesUploadaddpost.js",
    "groupTitle": "文件上传"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/update",
    "title": "更新用户数据",
    "description": "<p>&quot;&quot;</p>",
    "name": "更新用户数据",
    "group": "更新用户数据",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_info_id",
            "description": "<p>发送用户ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\n   \"status\":\"200\"\n   \"data\":\"修改成功\"\n                 \n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/update"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_infoupdatepost.js",
    "groupTitle": "更新用户数据"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/register",
    "title": "注册",
    "description": "<p>&quot;&quot;</p>",
    "name": "注册",
    "group": "注册",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_info_id",
            "description": "<p>用户ID --如果修改用户的话</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "head",
            "description": "<p>头像</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mail",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "surname",
            "description": "<p>姓</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "county",
            "description": "<p>县</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>城</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "town",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "district",
            "description": "<p>区</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>完整地址</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>国家</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>角色ID 注册默认为1</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\n   \"data\":\"如果是数组的话 表示有用户 请选择用户人进行覆盖\"\n                 \n }",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/register"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_inforegisterpost.js",
    "groupTitle": "注册"
  },
  {
    "type": "ws",
    "url": "/ws://xxxxx:8030",
    "title": "用户聊天soket",
    "description": "<p>&quot;&quot;</p>",
    "name": "用户聊天soket",
    "group": "用户聊天soket",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_id",
            "description": "<p>发送人用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>信息内容 all(获取全部信息) send(发送信息)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "copy_user_id",
            "description": "<p>接收人用户ID</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content_type",
            "description": "<p>内容类型  1普通文本  2图片信息  3文件信息 4富文本信息</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/ws://xxxxx:8030"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/wsxxxxx8030ws.js",
    "groupTitle": "用户聊天soket"
  },
  {
    "type": "post",
    "url": "/sphy_user_info/login",
    "title": "登录",
    "description": "<p>&quot;&quot;</p>",
    "name": "登录",
    "group": "登录",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "mail",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/sphy_user_info/login"
      }
    ],
    "version": "0.0.0",
    "filename": "DaveFile/database/documents/sphy_user_infologinpost.js",
    "groupTitle": "登录"
  }
] });
